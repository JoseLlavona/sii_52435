#include "DatosMemCompartida.h"
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(){
	DatosMemCompartida* p_Datos;
	int fd;
	fd = open ("/tmp/fichero", O_RDWR|O_CREAT, 0666);
	if(fd<0){
		perror("Error abrir fichero bot");
		return 1;
	}
	ftruncate (fd, sizeof(DatosMemCompartida)*2);
	p_Datos = static_cast<DatosMemCompartida*> (mmap( NULL, sizeof (DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	close (fd);
	
	
	while(1){
		int esfera_x= p_Datos->esfera.centro.x;
		int esfera_y= p_Datos->esfera.centro.y;
		int raqueta1_y= (p_Datos->raqueta1.y2+p_Datos->raqueta1.y1)/2;
		int raqueta2_y= (p_Datos->raqueta2.y2+p_Datos->raqueta2.y1)/2;

		usleep (25000);
		if(esfera_x>0){		// BOT 2 sigue esfera, BOT 1 vuelve al centro
			if (raqueta1_y > 1) p_Datos->accion1 = -1;
			else if (raqueta1_y < -1) p_Datos->accion1 = 1;
			else p_Datos->accion1 = 0;

			if (raqueta2_y > esfera_y) p_Datos->accion2 = -1;
			else if (p_Datos->raqueta2.y2 < esfera_y) p_Datos->accion2 = 1;
			else p_Datos->accion2 = 0;
		}
		if(esfera_x< 0){		// BOT 1 sigue esfera, BOT 2 vuelve al centro
			if (raqueta1_y > esfera_y) p_Datos->accion1 = -1;
			else if (raqueta1_y < esfera_y) p_Datos->accion1 = 1;
			else p_Datos->accion1 = 0;

			if (raqueta2_y > 1) p_Datos->accion2 = -1;
			else if (p_Datos->raqueta2.y2 < -1) p_Datos->accion2 = 1;
			else p_Datos->accion2 = 0;
		}
	
	} 
	return 0;
}
