#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> 


int main(){
  int player;
  int score;
  mkfifo ("/tmp/mififo", 0666);
  int fd= open ("/tmp/mififo", O_RDONLY);
  while (read (fd, &player, sizeof(int))== sizeof(int)){    
    read (fd, &score, sizeof(int));
    printf("\nPUNTO DEL JUGADOR %d!\nLa puntuacion del jugador %d es: %d\n", player, player, score);
    if (score ==3){
      printf("\nEl jugador %d ha ganado!", player);
      close (fd);
      unlink ("/tmp/mififo");
    }
  }
  return (0);
}
